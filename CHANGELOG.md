# phpcs-security-audit changelog

## v2.11.0
- Update report dependency in order to use the report schema version 14.0.0 (!52)

## v2.10.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!50)

## v2.10.0
- Update common to v2.22.0 (!49)
- Update urfave/cli to v2.3.0 (!49)

## v2.9.1
- Update composer to v2.0.7 (!48)
- Update logrus, testify, cli, and common golang dependencies to latest versions

## v2.9.0
- Update common and enable disablement of rulesets (!46)

## v2.8.2
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!41)

## v2.8.1
- Update golang dependencies (!40)

## v2.8.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!37)

## v2.7.1
- Upgrade go to version 1.15 (!36)

## v2.7.0
- Add scan object to report (!32)

## v2.6.0
- Switch to the MIT Expat license (!29)

## v2.5.0
- Update logging to be standardized across analyzers (!28)

## v2.4.3
- Remove `location.dependency` from the generated SAST report (!27)

## v2.4.2
- Update phpcs-security-audit to 2.0.1 (!25)

## v2.4.1
- Fix Docker image to allow php user to write to custom CA bundle file (!26)

## v2.4.0
- Added New CLI Flags:  **extensions** -- Comma seperated list of additional PHP file extensions to pass into the processor (!20)

## v2.3.1
- Use upstream Composer base image (!21)

## v2.3.0
- Add description to issue (!19)

## v2.2.0
- Add `id` field to vulnerabilities in JSON report (!18)

## v2.1.0
- Add support for custom CA certs (!16)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Add `Scanner` property and deprecate `Tool`

## v1.2.0
- Show command error output

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
